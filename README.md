**Simple blockchain built with Flask**

Run server: python3 blockchain.py

---

## Mining

Send a GET request to http://localhost:5000/mine
e.g. curl http://localhost:5000/mine

---

## Transacting

Send a POST request to http://localhost:5000/transactions/new
e.g. curl -X POST -H "Content-Type: application/json" -d '{
 "sender": "d4ee26eee15148ee92c6cd394edd974e",
 "recipient": "someone-other-address",
 "amount": 5
}' "http://localhost:5000/transactions/new"

---

## Inspecting the full chain

Send a GET request to http://localhost:5000/chain
e.g. curl http://localhost:5000/chain

---

## Example of a single block

block = {
    'index': 1,
    'timestamp': 1506057125.900785,
    'transactions': [
        {
            'sender': "8527147fe1f5426f9dd545de4b27ee00",
            'recipient': "a77f5cdfa2934df3954a5c7c7da5df1f",
            'amount': 5,
        }
    ],
    'proof': 324984774000,
    'previous_hash': "2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824"
}

---

## Consensus Algorithm

Spin up more nodes, mine and transact, then make a GET request to /nodes/resolve

e.g. curl http://localhost:5000/nodes/resolve
